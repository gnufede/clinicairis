User-agent: *
Disallow: /copiaSeguridad/
Disallow: /EnriMon/
Disallow: /INNER/
Disallow: /innerxxi/
Disallow: /innerXXI_files/
Disallow: /iris/
Disallow: /iris-old/
Disallow: /nueva/
Disallow: /OLD/
Disallow: /snowy/
Disallow: /tomboyonline/
Disallow: /wordpress/
Disallow: /1index.html
Disallow: /abrazados.JPG
Disallow: /cvnicolasmontrotti.pdf
Disallow: /estilo.css
Disallow: /HPIM00.JPG
Disallow: /HPIM0013.JPG
Disallow: /indexant.html
Disallow: /InnerXXI.html
Disallow: /InnerXXI.tar.gz
Disallow: /J2EETutorial.pdf
Disallow: /popup2.htm
Disallow: /Ver

Sitemap: http://clinicairistrotti.com/sitemap.txt

